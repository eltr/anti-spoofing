import "./App.css";

import { Switch, Route } from "react-router-dom";
import Instruction from "./ui-component/instruction/Instruction";

function App() {
  return (
    <>
      <Switch>
        <Route exact path="/" component={Instruction} />
      </Switch>
    </>
  );
}

export default App;
