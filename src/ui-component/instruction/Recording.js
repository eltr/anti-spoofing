/* eslint-env browser */
import React, { createRef } from "react";
import axios from "axios";

// audio
import silent from "../../asset/audio/silent.m4a";
import down from "../../asset/audio/down.mp3";
import up from "../../asset/audio/up.mp3";
import right from "../../asset/audio/right.mp3";
import left from "../../asset/audio/left.mp3";
import smile from "../../asset/audio/smile.mp3";

// lottie files
import lottie from "lottie-web";
import Lottie from "react-lottie";
import IconSimle from "../../asset/iconsmile.json";
import TurnIcon from "../../asset/lf30_editor_lvkte0ct.json";
import Loading from "../../asset/loading.json";
import CountDown from "../../asset/countdown.json";
import Alert from "@material-ui/lab/Alert";

import { Button } from "@material-ui/core";
const videoType = "video/webm";

export default class Recording extends React.Component {
  constructor(props) {
    super(props);

    this.isComponentMounted = false;

    this.state = {
      recording: false,
      data: File,
      videos: [],
      number: -1, // cơ sở để ẩn hiện 5 pose
      testCheck: 0,
      start: true, // cơ sở để ẩn hiện nút quay khi user ấn quay
      result: "", // cơ sở để ẩn hiện kết quả
      random: [], // cơ sở để RLUDS hoặc ...
      trialId: "", // mỗi lần quay sẽ tạo 1 trialId
      headpose_api: "", // lưu api headpose khi submit
      response_api: "", // lưu kết quả phản hồi headpose
      feedback: 0, // ẩn hiện yes no
      finish_audio: false, // cơ sở để chuyển pose
      show_pose: false, // cơ sở để hiển thị pose
    };
  }

  container = createRef(null);
  container2 = createRef(null);

  async componentDidMount() {
    this.isComponentMounted = true;

    const stream = await navigator.mediaDevices.getUserMedia({
      video: true,
    });
    // show it to user
    this.video.srcObject = stream;
    this.video.play();
    // init recording
    this.mediaRecorder = new MediaRecorder(stream, {
      mimeType: videoType,
    });
    // init data storage for video chunks
    this.chunks = [];
    // listen for data from media recorder
    this.mediaRecorder.ondataavailable = (e) => {
      if (e.data && e.data.size > 0) {
        this.chunks.push(e.data);
      }
    };

    lottie.loadAnimation({
      container: this.container.current,
      renderer: "svg",
      loop: true,
      autoplay: true,
      animationData: require("../../asset/lf30_editor_lvkte0ct.json"),
    });
    lottie.loadAnimation({
      container: this.container2.current,
      renderer: "svg",
      loop: true,
      autoplay: true,
      animationData: require("../../asset/iconsmile.json"),
    });

    // create track_id and save localStorage
    let textId1 = "";
    let textId2 = "";
    let possible1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    let possible2 = "0123456789";

    for (let i = 0; i < 3; i++)
      textId1 += possible1.charAt(Math.floor(Math.random() * possible1.length));

    for (let i = 0; i < 3; i++)
      textId2 += possible2.charAt(Math.floor(Math.random() * possible2.length));

    if (!localStorage.getItem("TRACK_ID")) {
      localStorage.setItem("TRACK_ID", JSON.stringify(textId1 + textId2));
    }
    console.log(JSON.parse(localStorage.getItem("TRACK_ID")));
  }

  async startRecording(e) {
    e.preventDefault();
    this.setState({ result: " " });
    this.setState({ start: false });
    this.setState({ feedback: 0 });
    // clear trialId, headpose_api, response_api
    await this.setState({ trialId: " " });
    await this.setState({ headpose_api: " " });
    await this.setState({ response_api: " " });

    // wipe old data chunks
    this.chunks = [];
    // start recorder with 10ms buffer
    this.mediaRecorder.start(10);
    // say that we're recording
    this.setState({ recording: true });

    // random trial_id
    let trialId = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    for (let i = 0; i < 4; i++)
      trialId += possible.charAt(Math.floor(Math.random() * possible.length));

    await this.setState({ trialId });

    // random pose
    var listRandomPoseAudio = [1, 2, 3, 4, 5];
    listRandomPoseAudio = listRandomPoseAudio.sort(() => Math.random() - 0.5);
    listRandomPoseAudio.unshift(0);
    this.setState({ random: listRandomPoseAudio });

    var index = 0;

    var action = setInterval(() => {
      console.log(listRandomPoseAudio);
      if (index !== 6) {
        this.setState({ number: listRandomPoseAudio[index] });
        console.log(this.state.finish_audio);
        console.log(listRandomPoseAudio[index]);
        if (this.state.finish_audio === true) {
          index = index + 1;
          if (index !== 6) {
            this.setState({ number: listRandomPoseAudio[index] });
            console.log("hiện pose", listRandomPoseAudio[index]);
            this.setState({ finish_audio: false });
          } else {
            this.setState({ finish_audio: false });
            clearInterval(action);
            this.setState({ number: -1 });
            this.stopRecording(e);
            this.setState({ testCheck: 1 });
            this.submit();
            index = 0;
          }
        }
      }
    }, 2000);
  }

  stopRecording(e) {
    e.preventDefault();
    // stop the recorder
    this.mediaRecorder.stop();
    // say that we're not recording
    this.setState({ recording: false });
    // save the video to memory
    this.saveVideo();
  }

  saveVideo() {
    // convert saved chunks to blob
    const data = new Blob(this.chunks, { type: videoType });

    // generate video url from blob
    const videoURL = window.URL.createObjectURL(data);
    // append videoURL to list of saved videos for rendering
    const videos = this.state.videos.concat([videoURL]);
    this.setState({ videos });
    this.setState({ data });
  }

  deleteVideo(videoURL) {
    // filter out current videoURL from the list of saved videos
    const videos = this.state.videos.filter((v) => v !== videoURL);
    this.setState({ videos });
  }

  async submit(i) {
    // convert Blob URL to file object with axios
    var dataBlob;
    const config = { responseType: "blob" };
    await axios
      .get(
        window.URL.createObjectURL(new Blob(this.chunks, { type: videoType })),
        config
      )
      .then((response) => {
        dataBlob = new File([response.data], "video", { type: "media" });
      });

    // submit
    var filename = "input.mp4";
    const myFileInput = {
      files: [],
    };
    myFileInput.files.push(new File([dataBlob], filename));

    var sequence = "";
    console.log(this.state.random);
    for (var se = 1; se < this.state.random.length; se++) {
      if (this.state.random[se] === 1) {
        sequence = sequence + "R";
      }
      if (this.state.random[se] === 2) {
        sequence = sequence + "L";
      }
      if (this.state.random[se] === 3) {
        sequence = sequence + "U";
      }
      if (this.state.random[se] === 4) {
        sequence = sequence + "D";
      }
      if (this.state.random[se] === 5) {
        sequence = sequence + "S";
      }
    }
    console.log(sequence);

    console.log(this.state.trialId);
    console.log(JSON.parse(localStorage.getItem("TRACK_ID")));
    console.log(
      `${JSON.parse(localStorage.getItem("TRACK_ID"))}_${this.state.trialId}`
    );

    if (sequence !== "") {
      var formdata = new FormData();
      formdata.append("sequence", sequence);
      formdata.append("video", myFileInput.files[0], filename);
      formdata.append(
        "unique_id",
        `${JSON.parse(localStorage.getItem("TRACK_ID"))}_${this.state.trialId}`
      );

      var requestOptions = {
        method: "POST",
        body: formdata,
        redirect: "follow",
      };

      // random 1 or 2, nếu 1 use https://headpose4, 2 use https://headpose5
      const randomPort = Math.random();
      console.log(randomPort);
      var link = "";
      if (randomPort > 0.5) {
        link = "https://headpose4.eyeq.tech/headpose";
      } else {
        link = "https://headpose5.eyeq.tech/headpose";
      }
      this.setState({ headpose_api: link });
      await this.requestHeadPose(requestOptions, link);
    }
  }

  async requestHeadPose(requestOptions, link) {
    await fetch(link, requestOptions)
      .then((response) =>
        response.text().then((text) => {
          console.log(JSON.parse(text));

          if (this.isComponentMounted) {
            this.setState({ response_api: JSON.parse(text) });
            this.setState({ headpose_api: link });
          }

          if (text.includes("fail") === true) {
            this.setState({ result: "fail" });
          } else if (text.includes("success") === true) {
            this.setState({ result: "success" });
          } else {
            this.setState({ result: "fail" });
          }

          // POST API hpstats.eyeq.tech
          this.reqHpstats(text, link, "");
        })
      )
      .then((result) => console.log(result))
      .catch((error) => {
        console.log("error", error);
        // POST API hpstats.eyeq.tech
        this.reqHpstats(error, link, "");
      });
    this.setState({ testCheck: 0 });
    this.setState({ feedback: 1 }); // hiển thị thông bao yes no
    this.setState({ start: true });
  }

  reqHpstats(text, link, feedback) {
    const filename = "hpfeedback.json";
    const myFileInput = {
      files: [],
    };
    myFileInput.files.push(new File([text], filename));

    var formdata_hpstats = new FormData();
    formdata_hpstats.append("result_object", myFileInput.files[0], filename);
    formdata_hpstats.append("headpose_link", link);
    formdata_hpstats.append(
      "track_id",
      JSON.parse(localStorage.getItem("TRACK_ID"))
    );
    formdata_hpstats.append("trial_id", this.state.trialId);
    formdata_hpstats.append("feedback", feedback);

    let requestOptions = {
      method: "POST",
      body: formdata_hpstats,
      redirect: "follow",
    };

    fetch("https://hpstats.eyeq.tech", requestOptions)
      .then((response) => response.text())
      .then((result) => console.log(result))
      .catch((error) => console.log("error", error));
  }

  async yes() {
    this.setState({ feedback: 0 });

    await this.reqHpstats(
      this.state.response_api,
      this.state.headpose_api,
      "yes"
    );
  }
  async no() {
    this.setState({ feedback: 0 });
    await this.reqHpstats(
      this.state.response_api,
      this.state.headpose_api,
      "no"
    );
  }

  userStart() {
    this.setState({ userStart: true });
  }

  componentWillUnmount() {
    this.isComponentMounted = false;
  }

  render() {
    const defaultOptions = {
      loop: 1,
      autoplay: true,
      animationData: TurnIcon,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
    };
    const smileOptions = {
      loop: 1,
      autoplay: true,
      animationData: IconSimle,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
    };
    const loadingOptions = {
      loop: true,
      autoplay: true,
      animationData: Loading,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
    };
    const countOptions = {
      loop: false,
      autoplay: true,
      animationData: CountDown,
      rendererSettings: {
        preserveAspectRatio: "xMidYMid slice",
      },
    };

    return (
      <div className="camera">
        <div className="formRecording">
          <span className="detailInstruction">
            <div className="red">
              <i>*Chỉ hoạt động trên Android và Chrome (vui lòng bật loa)</i>
            </div>
            <div>
              <i>
                Hãy chọn môi trường đủ sáng thấy rõ mặt, không bị chói nắng/đèn
                phía sau, không đeo kính. Hãy quay đầu theo hưỡng mũi tên chỉ,
                giữ 1 giây, sau đó quay thẳng lại, và lại quay theo hướng mũi
                tên chỉ tiếp theo. Khi nào gặp{" "}
                <span className="bold">mặt cười</span>, hãy cười nhoẻn miệng
                thật rộng theo chiều ngang, để lộ răng.<br></br>
                Làm lần đầu thường không quen, hãy thử lại nếu sai
              </i>
            </div>
          </span>
          <br></br>
          {this.state.start === true ? (
            <Button
              onClick={(e) => this.startRecording(e)}
              variant="outlined"
              color="primary"
            >
              Bắt đầu
            </Button>
          ) : (
            ""
          )}
        </div>
        <br></br>
        {this.state.result === "fail" ? (
          <Alert severity="error">
            Nghi ngờ giả do không thực hiện theo yêu cầu. ID:{" "}
            {this.state.trialId}
          </Alert>
        ) : (
          ""
        )}
        {this.state.result === "success" ? (
          <Alert severity="success">Mặt thật. ID: {this.state.trialId}</Alert>
        ) : (
          ""
        )}
        {this.state.testCheck === 1 ? (
          <Alert severity="warning">
            Thời gian trả kết quả: 20 - 30 giây. ID: {this.state.trialId}
          </Alert>
        ) : (
          ""
        )}
        <div className="borderCircular">
          <div className="turn">
            <video
              className="video"
              ref={(v) => {
                this.video = v;
              }}
            ></video>
            {this.state.number === 0 ? (
              <div className="count">
                <audio
                  autoPlay
                  onEnded={() => {
                    this.setState({ finish_audio: true });
                  }}
                >
                  <source src={silent}></source>
                </audio>
                <Lottie options={countOptions} height={150} width={150} />
              </div>
            ) : (
              ""
            )}
            {this.state.number === 1 ? (
              <div className="tRight">
                <audio
                  autoPlay
                  onPlay={() => {
                    this.setState({ show_pose: true });
                  }}
                  onEnded={() => {
                    this.setState({ show_pose: false });
                    this.setState({ finish_audio: true });
                  }}
                >
                  <source className="srcAudio" src={right}></source>
                </audio>
                {this.state.show_pose === true ? (
                  <Lottie options={defaultOptions} height={60} width={120} />
                ) : (
                  ""
                )}
              </div>
            ) : (
              ""
            )}
            {this.state.number === 2 ? (
              <div className="tLeft">
                <audio
                  autoPlay
                  onPlay={() => {
                    this.setState({ show_pose: true });
                  }}
                  onEnded={() => {
                    this.setState({ show_pose: false });
                    this.setState({ finish_audio: true });
                  }}
                >
                  <source className="srcAudio" src={left}></source>
                </audio>
                {/* <Lottie options={defaultOptions} height={60} width={120} /> */}
                {this.state.show_pose === true ? (
                  <Lottie options={defaultOptions} height={60} width={120} />
                ) : (
                  ""
                )}
              </div>
            ) : (
              ""
            )}
            {this.state.number === 3 ? (
              <div className="tUp">
                <audio
                  autoPlay
                  onPlay={() => {
                    this.setState({ show_pose: true });
                  }}
                  onEnded={() => {
                    this.setState({ show_pose: false });
                    this.setState({ finish_audio: true });
                  }}
                >
                  <source className="srcAudio" src={up}></source>
                </audio>
                {/* <Lottie options={defaultOptions} height={60} width={120} /> */}
                {this.state.show_pose === true ? (
                  <Lottie options={defaultOptions} height={60} width={120} />
                ) : (
                  ""
                )}
              </div>
            ) : (
              ""
            )}
            {this.state.number === 4 ? (
              <div className="tDown" id="cui">
                <audio
                  autoPlay
                  onPlay={() => {
                    this.setState({ show_pose: true });
                  }}
                  onEnded={() => {
                    this.setState({ show_pose: false });
                    this.setState({ finish_audio: true });
                  }}
                >
                  <source className="srcAudio" src={down}></source>
                </audio>
                {/* <Lottie options={defaultOptions} height={60} width={120} /> */}
                {this.state.show_pose === true ? (
                  <Lottie options={defaultOptions} height={60} width={120} />
                ) : (
                  ""
                )}
              </div>
            ) : (
              ""
            )}
            {this.state.number === 5 ? (
              <div className="tSmile">
                <audio
                  autoPlay
                  onPlay={() => {
                    this.setState({ show_pose: true });
                  }}
                  onEnded={() => {
                    this.setState({ show_pose: false });
                    this.setState({ finish_audio: true });
                  }}
                >
                  <source className="srcAudio" src={smile}></source>
                </audio>
                {/* <Lottie options={smileOptions} height={130} width={130} /> */}
                {this.state.show_pose === true ? (
                  <Lottie options={smileOptions} height={120} width={120} />
                ) : (
                  ""
                )}
              </div>
            ) : (
              ""
            )}
            {this.state.testCheck === 1 ? (
              <div className="loading">
                <Lottie options={loadingOptions} height={130} width={130} />
              </div>
            ) : (
              ""
            )}
          </div>
        </div>

        {this.state.feedback === 1 ? (
          <div className="feedbackForm">
            <div className="formBtn">
              <span>Bạn có đồng ý với kết quả này không?</span>
            </div>
            <div className="formBtn">
              <div>
                <Button onClick={() => this.yes()} color="primary">
                  Có
                </Button>
              </div>
              <div>
                <Button onClick={() => this.no()} color="secondary">
                  Không
                </Button>
              </div>
            </div>
          </div>
        ) : (
          ""
        )}
      </div>
    );
  }
}
