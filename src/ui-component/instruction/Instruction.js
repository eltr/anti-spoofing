import React from "react";

// import VideoPlayer from "react-video-js-player";
// import VideoInstruction from "../../asset/Video-Instruction.mp4";
// import PosterInstruction from "../../asset/Poster-Instruction.png";
import logo from "../../asset/logo.png";
import Recording from "./Recording";
// material-ui
// import SkipNextTwoToneIcon from "@material-ui/icons/SkipNextTwoTone";
// import { Button } from "@material-ui/core";

// {
//   /* <VideoPlayer
//   src={VideoInstruction}
//   poster={PosterInstruction}
//   width="720"
//   height="420"
// />; */
// }

const Instruction = () => {
  return (
    <>
      <div className="instruction">
        <div className="sideBar">
          <div className="borderLogo">
            <img className="logo" src={logo} alt="logo eyeq" />
          </div>
          <div className="titleSideBar"></div>
        </div>
        <div className="content">
          <div className="formBorder">
            <div className="borderAniSpoofing">
              <h4 className="antiSpoofing">
                Hoạt động Kiểm thử chức năng xác thực chủ thể sống theo yêu cầu
                của VietCredit - thực hiện bởi Cty EyeQ Tech
              </h4>
              <Recording />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Instruction;
