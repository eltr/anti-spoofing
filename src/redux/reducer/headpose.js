const stateDefault = {};

const headpose = (state = stateDefault, action) => {
  switch (action.type) {
    default:
      return { ...state };
  }
};

export default headpose;
