import headpose from "./headpose";

import { combineReducers } from "redux";
export const rootReducer = combineReducers({
  headpose,
});
